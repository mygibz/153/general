# M153 - Datenmodelle entwickeln

**Kompetenz:** Kundenanforderungen für Informationen und Informationsbestände aufnehmen, analysieren und Datenmodell(e) entwickeln.

**Handlungsziele**

1. Informationsbedürfnisse und Anforderungen an die Datenhaltung zusammen mit dem Auftraggeber aufnehmen (z.B. Geschäftsfälle), analysieren, sowie Schutz- und Sicherheitsbedürfnisse der Daten definieren und dokumentieren.   

    ***Handlungsnotwendige Kenntnisse:***

    1. Kennt die verbreiteten Methoden (z.B. Studium Dokumentationen, Interview) zur Erhebung von Informationen in einem Betrieb und kann erläutern, für welche Arten von Informationen und bei welchen betrieblichen Rahmenbedingungen diese geeignet sind.

 

2. Konzeptionelles Datenmodell erstellen und fehlende Informationen ermitteln bzw. Redundanzen klären. 

    **Handlungsnotwendige Kenntnisse:**

    1. Kennt die methodische Vorgehensweise bei der Entwicklung eines konzeptionellen Datenmodells und kann für die einzelnen Schritte erläutern, wie dabei vorgegangen wird.

    2. Kennt die Regeln, welche beim Spezialisieren bzw. Generalisieren von Objekten in Entitätsmengen und Beziehungen zu beachten sind.

 

3. Konzeptionelles Datenmodell in ein logisches überführen und Attribute, Identifikations- und Fremdschlüssel, Datentypen ergänzen.    

    **Handlungsnotwendige Kenntnisse:**

    1. Kennt die spezifischen Merkmale, welche ein Identifikationsschlüssel in einem Datenmodell erfüllen muss und kann erläutern, welche Verarbeitungsmöglichkeiten damit sichergestellt werden.

    2. Kennt die erweiterten Konstruktionselemente des ER Modells (Rekursive Assoziation) und der Datenmodellierung (Generalisierung/Spezialisierung) und kann aufzeigen, bei welchen Sachverhalten diese in der Datenmodellierung eingesetzt werden.

    3. Kennt die Schritte der Normalisierung bis zur 3. Normalform und kann an Beispielen erläutern, welchen Beitrag diese zu einer redundanzfreien Datenhaltung beitragen.

    4. Kennt die verschiedenen Kardinalitäten (Assoziationen) und kann erläutern, wie diese mittels der Definition von Primär- und Fremdschlüsseln in einem logischen Datenmodell umzusetzen sind. (aus 5.3)

 

4. Konzeptionelles Datenmodell zusammen mit dem Auftraggeber überprüfen und allfällige Anpassungen vornehmen.  

    **Handlungsnotwendige Kenntnisse:**

    1. Kennt den Einsatz einer Reviewtechnik um das Datenmodell mit dem Auftraggeber zu verifizieren.

    2. Kennt Einsatzmöglichkeiten eines Prototyps, um in der Diskussion mit dem Auftraggeber die Anforderungen zu überprüfen und zu konkretisieren.

 

5. Für das vorliegende logische Datenmodell das physikalische Datenbankschema entwickeln und implementieren.   

    **Handlungsnotwendige Kenntnisse:**

    1. Kennt Möglichkeiten, wie die Zugriffszeiten auf eine Datenbank durch die Definition von Indizes und durch Denormalisierung (kontrollierte Redundanz) von Tabellen verbessert werden können und kann anhand von Beispielen erläutern, wie diese Vorkehrungen zur Verbesserung dieser Zugriffszeiten beitragen.

 

Kompetenzfeld: Data Management

Objekt: Anspruchsvolle Kundenanforderungen (komplexe und rekursive Beziehungstypen).

Niveau: 4

Voraussetzung: Datenmodell implementieren

Anzahl Lektionen: 40

Anerkennung: Eidg. Fähigkeitszeugnis
