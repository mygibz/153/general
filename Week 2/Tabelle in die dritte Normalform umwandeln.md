# Tabelle in die dritte Normalform umwandeln

### Ausgangslage:

| ArtikelNr | Bezeichnung                | Preis | Von        | Bis        |
| --------- | -------------------------- | ----- | ---------- | ---------- |
| 1001      | Digital-Kamera Vision 1.3M | 850   | 01.01.2015 | 31.03.2015 |
|           |                            | 650   | 01.04.2015 | 30.06.2015 |
|           |                            | 350   | 01.07.2015 |            |
| 1002      | Digital-Kamera HighPix 4   | 1950  | 01.01.2016 | 31.03.2016 |
|           |                            | 1400  | 01.04.2016 | 30.06.2016 |
| 1003      | Scanner 2400i              | 320   | 01.01.2016 | 31.03.2016 |
| 1004      | Drucker ABC                | 250   | 01.02.2016 | 30.04.2016 |



### Resultat

**Artikel**

| ArtikelNr | Bezeichnung                |
| --------- | -------------------------- |
| 1001      | Digital-Kamera Vision 1.3M |
| 1002      | Digital-Kamera HighPix 4   |
| 1003      | Scanner 2400i              |
| 1004      | Drucker ABC                |

**Preise** (man sollte hier eigentlich eine PreisID setzen)

| ArtikelNr | Preis | Von        | Bis        |
| --------- | ----- | ---------- | ---------- |
| 1001      | 850   | 01.01.2015 | 31.03.2015 |
| 1001      | 650   | 01.04.2015 | 30.06.2015 |
| 1001      | 350   | 01.07.2015 |            |
| 1002      | 1950  | 01.01.2016 | 31.03.2016 |
| 1002      | 1400  | 01.04.2016 | 30.06.2016 |
| 1003      | 320   | 01.01.2016 | 31.03.2016 |
| 1004      | 250   | 01.02.2016 | 30.04.2016 |

